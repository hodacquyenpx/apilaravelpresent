@extends('layouts.app')


@section('content')
  <form action="{{ route('save') }}" method="POST" enctype="multipart/form-data">
   @csrf
    <div class="card">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-5">
                    <h4 class="card-title mb-0">
                        Upload Image
                    </h4>
                </div><!--col-->
            </div><!--row-->

            <hr>

            <div class="row">
              <div class="col-lg-12">
                  <div class="form-group">
                      <label class="form-control-label" for="price">Image</label>
                      <input type="file" id="image" required class="form-control numeric form-control-alternative @error('image') is-invalid @enderror"  name="image"  accept="image/x-png,image/gif,image/jpeg" >
                  </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12">
                  <div class="form-group">
                      <label class="form-control-label" for="name">Name</label>
                      <input type="text" id="name" required class="form-control" placeholder="Name" name="name" >
                  </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-12">
                  <div class="form-group">
                      <label class="form-control-label" for="name">Email</label>
                      <input type="email" id="email" required class="form-control" placeholder="Email" name="email" >
                  </div>
              </div>
            </div>
        </div><!--card-body-->

        <div class="card-footer">
            <div class="row">
                <div class="col text-right">
                  <input type="submit" name="submit" value="submit">
                </div><!--row-->
            </div><!--row-->
        </div><!--card-footer-->
    </div><!--card-->
  </form>
@endsection
