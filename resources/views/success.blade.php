@extends('layouts.app')

@section('content')
  <h1 class="list_user">List User</h1>
  <table class="table table-bordered" id="users-table">
      <thead>
          <tr>
              <th>Id</th>
              <th>Image</th>
              <th>Name</th>
              <th>Email</th>
              <th>Created At</th>
              <th>Updated At</th>
          </tr>
      </thead>
  </table>
@endsection
