<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Image;
use DataTables;

/**
 * Class HomeController.
 */
class HomeController extends Controller
{
    public function show()
    {
        return view('upload');
    }

    public function showImage($file = '')
    {
        if (isset($file) && $file) {
            $hostSaveImage = env('SAVE_IMAGE_URL');
            $linkImage = $hostSaveImage.'/'.$file;

            $headers = @get_headers($linkImage);
            // Use condition to check the existence of URL
            if ($headers && strpos($headers[0], '200')) {
                return view('success', compact('linkImage'));
            }

            return redirect()->route('home')
                ->withFlashDanger('Not Found Image!');
        }

        return redirect()->route('home')
            ->withFlashDanger('Not Found Image!');
    }


    public function getUsers()
    {
        return Datatables::of(Image::query())->make(true);
    }
}
