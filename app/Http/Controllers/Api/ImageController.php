<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ImageController extends Controller
{
    /**
     * Login.
     *
     * @param LoginRequest $request
     * @throws \Illuminate\Validation\ValidationException
     * @return \Illuminate\Http\JsonResponse
     */
    public function save(Request $request)
    {
        // host
        $hostUploadImage = env('API_UPLOAD_URL');
        $hostSaveSql = env('API_SQL_URL');

        // file image
        $file = new \CURLFile($request->image->getRealPath(), $request->image->getClientOriginalExtension(), $request->image->getClientOriginalName());
        //Our string.
        $string = '123456789abcdefghijklmnopqrstuvwxyz';
        // random string
        $key = substr(str_shuffle($string), 0, 1).time();
        // name image
        $name_file = time().'_'.$key.'.'.request()->image->getClientOriginalExtension();

        // send upload image
        $curl = curl_init();

        curl_setopt_array($curl, [
          CURLOPT_URL => $hostUploadImage,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => ['image' => $file, 'name_file' => $name_file],
          CURLOPT_HTTPHEADER => [
              'Content-Type: multipart/form-data',
          ],
        ]);

        $saveImage = curl_exec($curl);
        // close curl
        curl_close($curl);

        // save data
        $ch = curl_init();
        $data = [
            'name' => $request->all()['name'],
            'email' => $request->all()['email'],
            'image' => $name_file,
        ];

        $data = json_encode($data);
        curl_setopt_array($ch, [
          CURLOPT_URL => $hostSaveSql,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => $data,
          CURLOPT_HTTPHEADER => [
              'Content-Type: application/json',
          ],
        ]);

        $saveSql = curl_exec($ch);

        curl_close($ch);

        if ($saveImage && $saveSql) {
            return redirect()->route('showImage', $name_file)
              ->withFlashSuccess('Upload Image Success!');
        }

        return redirect()->route('home')
          ->withFlashDanger('Upload Image Fail!');
    }
}
