
setTimeout(function(){
  $(".alert").css("display", "none");
}, 3000);

$(".close").click(function(){
  $(".alert").css("display", "none");
});

$(function() {
    $('#users-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: 'http://laravel-docker.tk/users',
        columns: [
            { data: 'id', name: 'id' },
            { data: 'image', name: 'image',
                render: function( data, type, full, meta ) {
                    return "<img src=\"http://media.laravel-docker.tk/images/" + data + "\" height=\"50\"/>";
                }
            },
            { data: 'name', name: 'name' },
            { data: 'email', name: 'email' },
            { data: 'created_at', name: 'created_at' },
            { data: 'updated_at', name: 'updated_at' }
        ]
    });
});
